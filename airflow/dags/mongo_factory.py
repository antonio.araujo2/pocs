import glob
from datetime import datetime, time, timedelta, timezone
from email.policy import default

import yaml
from airflow import DAG
from airflow.models import Variable as v
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonVirtualenvOperator
from eel import snowflake as sf
#from eel.airflow_callbacks import task_failure_slack_alert
from eel.helpers import day_iterator
from eel.logger import Logger
from eel.mongo import Mongo
from snowflake.connector import ProgrammingError
from datahub_provider.entities import Dataset

# Get Mongo configuration files
#mongo_files = glob.glob("dags/mongo/**/*.yaml")
mongo_files = glob.glob("dags/mongo/core/*.yaml")

# Function to iterate MongoDB results in batches
def iterate_by_chunks(collection, chunksize=50000, start_from=0, query={}):
    if type(collection) is list:
        for i in range(0, len(collection), chunksize):
            yield collection[i:i+chunksize]
    else:
        chunks = range(start_from, collection.count_documents(query), int(chunksize))
        num_chunks = len(chunks)

        for i in range(1, num_chunks + 1):
            if i < num_chunks:
                yield collection.find(query)[chunks[i - 1] : chunks[i]]
            else:
                yield collection.find(query)[chunks[i - 1] : chunks.stop]


# Function that takes the config and creates a DAG
def create_dag_mongo(config):

    # Initialize logging
    log = Logger(config["name"])

    # Initialize variables
    stage_name = config["mongo_collection"].upper()
    file_prefix = config["mongo_collection"].upper()

    # Define default arguments for DAG
    default_args = {
        "owner": config["owner"],
        "start_date": datetime.combine(config["start_date"], time.min),
        "retries": config["retries"],
        "retry_delay": timedelta(minutes=config["retry_delay_minutes"])
    }
    
    # Initialize the DAG
    dag = DAG(
        dag_id=config["nickname"],
        default_args=default_args,
        schedule_interval=None,
        #catchup=config["catchup"],
        catchup=False,
        max_active_runs=2,
        max_active_tasks=8,
        tags=config.get('tags', [])
    )

    def _upload_data(lower_limit=None, upper_limit=None, **context):
        # Initialize logging
        log.set_function("_sync_data")
        log.debug(f"config: {config}")

        # Log global variables
        log.debug(f"Stage Name: {stage_name}")
        log.debug(f"File Prefix: {file_prefix}")

        log.debug(f"Lower Limit: {lower_limit}")
        log.debug(f"Upper Limit: {upper_limit}")

        # Get date from context
        log.debug(f"Context: {context}")
        day_str = context["ds"]
        log.info(f"Day String: {day_str}")
        day = datetime.strptime(day_str, "%Y-%m-%d").date()
        log.info(f"Day: {day}")
        # 2023-05-19
        
        day_str = "2019-01-01"
        log.info(f"day_B String: {day_str}")
        day_B = datetime.strptime(day_str, "%Y-%m-%d").date()
        log.info(f"day_B: {day_B}")

        # Build interval dates
        initial_dt = datetime.combine(day_B, datetime.min.time()) 
        final_dt = datetime.combine(day, datetime.max.time()) 
        log.info(f"Start Datetime: {initial_dt}")
        log.info(f"End Datetime: {final_dt}")

        # Initialize Mongo
        log.info("Connect to MongoDB")
        if config.get('mongo_db_uri', False):
            db_uri = v.get(config['mongo_db_uri'])
            m = Mongo(db=config["mongo_db"], db_uri=db_uri)
        else:
            m = Mongo(config["mongo_db"])
        
        # Build aggregation pipeline for MongoDB
        log.info("Build aggregation pipeline for MongoDB")
        or_list = []
        
        # Check if there is a single field
        if "date_field" in config.keys():
            or_list.append(
                {
                    config["date_field"]: {
                        "$gte": initial_dt,
                        "$lt": final_dt,
                    }
                }
            )
        
        # Check if there are multiple date fields
        if "date_fields" in config.keys():
            for date_field in config["date_fields"]:
                or_list.append(
                    {
                        date_field: {
                            "$gte": initial_dt,
                            "$lt": final_dt,
                        }
                    }
                )
        
        # Get data from Mongo
        log.info("Get data from MongoDB")
        query = {"$or": or_list}
        log.info(f"Query: {query}")
        
        if "is_in_various_dbs" in config and config["is_in_various_dbs"]:
            collection = m.join_homonym_collections(config["mongo_collection"], query)
        else:
            collection = m.query()[config["mongo_collection"]]
        
        # Get the batch size
        try:
            batch_size = config["batch_size"]
        except KeyError:
            batch_size = 50000
        
        # Keep track of batch number
        batch_number = 0
        
        if config.get("full", False):
            query = {}
        
        # Iterate the query in batches
        '''for cursor in iterate_by_chunks(collection, query=query, chunksize=batch_size):
        
            log.info(f"Getting data for batch #{batch_number}")
        
            # Get data from cursor
            raw_data = list(cursor)
            log.debug(f"Data Length: {len(raw_data)}")
        
            # Check if there are any records
            if len(raw_data) < 1:
                log.warning("No records found. Exiting.")
                return "OK"
        
            # Upload data to Snowflake
            log.info("Upload data to Snowflake")
            prefix = (
                file_prefix
                + "_"
                + initial_dt.strftime("%Y%m%d%H%M%S")
                + f"_batch{batch_number}"
            )
            sf.list_stager(
                raw_data,
                stage_name,
                database=config["db_name"],
                schema=config["db_schema"],
                file_prefix=prefix,
                dumper=mongo_dumps,
            )
        
            batch_number += 1'''

        return "OK"

    def _copy_from_stage(**context):

        # Initialize logging
        log.set_function("_copy_from_stage")
        log.debug(f"config: {config}")

        # Log global variables
        log.debug(f"Stage Name: {stage_name}")
        log.debug(f"File Prefix: {file_prefix}")

        # Snowflake COPY INTO
        try:
            '''response = sf.copy_stage(
                stage_name,
                config["db_table"],
                database=config["db_name"],
                schema=config["db_schema"],
                if_exists="append",
                add_meta=True,
            )'''
        except ProgrammingError as pe:
            if pe.msg.endswith("does not exist or not authorized."):
                log.warning(pe.msg)
                log.warning(f"Will assume no records updated.")
                #response = (True, 0, 0, pe.msg)
            else:
                raise

        #success, chunks, rows, output = response

        '''# Log results
        log.debug(f"Success: {success}")
        log.debug(f"Chunks: {chunks}")
        log.debug(f"Rows: {rows}")
        log.debug(f"Output: {output}")

        # Check results
        if success:
            log.debug(f"Loaded {chunks} files in Snowflake inserting {rows} rows")
        else:
            log.debug("Error loading files to Snowflake")
            log.debug(response)
            raise RuntimeError("failed to load all files to Snowflake")'''

        return "OK"

    def _remove_duplicates(**context):
        # Initialize logging
        log.set_function("_remove_duplicates")
        log.debug(f"config: {config}")

        # Log global variables
        log.debug(f"Stage Name: {stage_name}")
        log.debug(f"File Prefix: {file_prefix}")

        # Set variables
        table_name = config["db_table"]

        # Create query
        remove_duplicates_sql = f"""
        -- Delete duplicates and keep the latest record
        delete from {table_name} t
        using (
            select JSON_DATA:"_id"."$oid"::text as id
                , max(concat(
                    to_char(INSERTED_AT, 'yyyymmddHH24MISSFF3')
                    , FILE_NAME
                    , FILE_ROW
                )) as deletion_key
            from {table_name}
            group by 1
        ) d
        where d.id = t.JSON_DATA:"_id"."$oid"::text
        and concat(
            to_char(t.INSERTED_AT, 'yyyymmddHH24MISSFF3')
            , t.FILE_NAME
            , t.FILE_ROW
        ) < d.deletion_key;
        """
        log.debug(f"Remove duplicates query: {remove_duplicates_sql}")

        # Execute query
        log.info("Execute query")
        '''response = sf.query(
            remove_duplicates_sql,
            database=config["db_name"],
            schema=config["db_schema"],
        )'''
        #log.debug(f"Response: {response}")

        return "OK"

    # Create a dummy start task
    start_task = DummyOperator(task_id="start", dag=dag)

    m_db = v.get(f"MONGO_DB_{config['mongo_db'].upper()}")

    # Copy files from stage into table
    copy_task = PythonOperator(
        task_id=f"copy_data",
        python_callable=_copy_from_stage,
        provide_context=True,
        dag=dag
    )

    # Create a dummy end task
    end_task = DummyOperator(task_id="end", dag=dag)

    # Check if we are running the de-duplication step
    if "skip_remove_duplicates" in config.keys() and config["skip_remove_duplicates"]:

        # Skip remove duplicates task
        copy_task.set_downstream(end_task)
        copy_task.outlets = [Dataset("snowflake", f"{config['db_name']}.{config['db_schema']}.{config['db_table']}".lower())]

    else:

        # Remove duplicates and keep latest
        remove_duplicates_task = PythonOperator(
            task_id=f"remove_duplicates",
            python_callable=_remove_duplicates,
            provide_context=True,
            dag=dag,                    
            outlets=[Dataset("snowflake", f"{config['db_name']}.{config['db_schema']}.{config['db_table']}".lower())]
        )
        copy_task.set_downstream(remove_duplicates_task)
        remove_duplicates_task.set_downstream(end_task)

    # Default: 24 hours interval (entire day load)
    interval = 24

    # Check if hours interval was given
    if "interval_hours" in config.keys():
        interval = config["interval_hours"] * 60

    # Check if minutes interval was given
    if "interval_minutes" in config.keys():
        interval = config["interval_minutes"]

    m_db = v.get(f"MONGO_DB_{config['mongo_db'].upper()}")

    # Upload files to stage
    upload_task = PythonVirtualenvOperator(
        task_id=f"upload_data",
        python_callable=_upload_data,
        provide_context=True,
        dag=dag,
        inlets=[Dataset("mongodb", f"{m_db}.{config['mongo_collection']}".lower())]
    )

    start_task.set_downstream(upload_task)
    upload_task.set_downstream(copy_task)

    return dag


for mongo_path in mongo_files:

    # Read YAML file
    with open(mongo_path, "r") as stream:
        config = yaml.safe_load(stream)

    # Add to global scope
    globals()[config["nickname"]] = create_dag_mongo(config)
