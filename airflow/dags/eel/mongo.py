import math
import os
from datetime import datetime

import pandas as pd
from airflow.models import Variable as v
from bson.objectid import ObjectId
# Import logger
from eel.logger import Logger
from pymongo import MongoClient

# Initialize logger
log = Logger("eel")
log.set_function("mongo")

# Database credentials
DB_AUTH = {
    "wasp": v.get("MONGO_URI_WASP"),
    "botana": v.get("MONGO_URI_INTEGRATIONS"),
    "core": v.get("MONGO_URI_CORE"),
    "buttons": v.get("MONGO_URI_BUTTONS"),
    "status": v.get("MONGO_URI_STATUS"),
    "logs": v.get("MONGO_URI_LOGS"),
    "marketing": v.get("MONGO_URI_MKT"),
    "d1": v.get("MONGO_URI_D1")
}

DB_NAME = {
    "wasp": v.get("MONGO_DB_WASP"),
    "botana": v.get("MONGO_DB_BOTANA"),
    "core": v.get("MONGO_DB_CORE"),
    "buttons": v.get("MONGO_DB_BUTTONS"),
    "status": v.get("MONGO_DB_STATUS"),
    "logs": v.get("MONGO_DB_LOGS"),
    "marketing": v.get("MONGO_DB_MKT"),
    "d1": v.get("MONGO_DB_D1")
}


class Mongo:
    def __init__(self, db, auth=DB_AUTH, db_uri=None):
        # Get credentials
        log.debug("Setting DB credentials")
        self.db = db
        if db_uri:
            self.db_uri = db_uri
            self.db_name = db
        else:
            if db not in auth:
                raise KeyError(
                    f"Unknown database key '{db}'. Provide an existing DB name or a valid URI."
                )
            self.db_uri = auth[db]
            self.db_name = DB_NAME[db]

    def __str__(self):
        if self._check_credentials():
            return f"<Mongo {self.db}>"
        else:
            return f"<Mongo NoCredentialsProvided>"

    def __repr__(self):
        if self._check_credentials():
            return f"<Mongo {self.db}>"
        else:
            return f"<Mongo NoCredentialsProvided>"

    def _check_credentials(self):
        if self.db_uri:
            return True
        else:
            return False

    def _connect(self):
        # Connecting to Mongo
        log.debug("Connecting to Mongo")
        self.client = MongoClient(self.db_uri)
        return self.client[self.db_name]

    def object_id_from_date_str(self, date_string, end_of_day=False):
        if end_of_day:
            date = date_string + " 23:59:59"
        else:
            date = date_string + " 00:00:00"
        d = datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
        return ObjectId.from_datetime(d)

    def query(self):
        if not self._check_credentials():
            raise ConnectionError("No credentials provided")
        db = self._connect()
        return db

    def join_homonym_collections(self, coll_name, query):
        if coll_name == None:
            raise KeyError(f"Unknown collection name key '{coll_name}'. Provide an existing collection name.")
        ## connect to server
        self.client = MongoClient(self.db_uri)
        dbs = self.client.list_database_names()
        dbs = list(filter(lambda x: not x.endswith('dev') and x.startswith('journey'), dbs))
        log.debug(f"List of dbs: {dbs}")
        joined_collection = []
        for db_name in dbs:
            db_conn = self.client[db_name]
            if coll_name in db_conn.list_collection_names():
                log.debug(f"{db_name} has a collection called {coll_name}")
                ## convert collection to json
                data = list(db_conn[coll_name].find(query))
                log.debug(f"{db_name}.{coll_name} has {len(data)} documents")
                if len(data) > 0:
                    ## add the business name as a field
                    business_name = db_name.split('_')[1]
                    for item in data:
                        item.update({'businessName': business_name})
                    log.info(f"The copy of {db_name}.{coll_name} was updated with the business name ({business_name}) in each item")
                    ## add the collection to the big json
                    joined_collection = joined_collection + data
        return joined_collection

    # def write(self, df, table_name, schema, if_exists="replace"):
    #     engine = self._connect()
    #     df.to_sql(table_name, engine, schema=schema, if_exists=if_exists)
    #     return True
