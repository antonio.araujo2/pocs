# !pip install pyarrow==0.17.0
# !pip install "snowflake-connector-python[pandas]"
# !pip install snowflake-sqlalchemy

import gzip
import json
import os
import random
import string
from datetime import datetime
from logging import getLogger

import pandas as pd
import snowflake.connector
from airflow.models import Variable as v
from pandas.api.types import is_datetime64_any_dtype as is_datetime
from snowflake.connector import ProgrammingError
from snowflake.connector.pandas_tools import pd_writer, write_pandas
from snowflake.sqlalchemy import URL, VARIANT
from sqlalchemy import create_engine
from sqlalchemy.dialects import registry

registry.register("snowflake", "snowflake.sqlalchemy", "dialect")

logger = getLogger(__name__)


# Snowflake credentials
SF_CREDENTIALS = {
    "account": v.get("SF_ACCOUNT"),
    "user": v.get("SF_USER"),
    "password": v.get("SF_PASSWORD"),
    "role": v.get("SF_ROLE"),
    "database": v.get("SF_DATABASE", "RAW"),
    "warehouse": v.get("SF_WAREHOUSE", "LOAD_WH"),
}

env = v.get("ENVIRONMENT")
logger.info(f"ENVIRONMENT: {env}")


def _test_connection(credentials=None):
    global SF_CREDENTIALS
    if not credentials:
        credentials = SF_CREDENTIALS
    """
    Test that the connection with Snowflake can be established
    Return the Snowflake version
    """

    sf_version = None  # Forward declaration

    # Create a SF connection
    connection = snowflake.connector.connect(
        account=SF_CREDENTIALS["account"],
        user=SF_CREDENTIALS["user"],
        password=SF_CREDENTIALS["password"],
    )

    # Create a cursor
    cursor = connection.cursor()

    try:
        # Execute query
        response = cursor.execute("SELECT current_version()").fetchone()
        # Store the result
        sf_version = response[0]
    finally:
        # In any case, close the cursor
        cursor.close()
        # Close the connection
        connection.close()

        return sf_version is not None


def connect(credentials=None, read=False):
    global SF_CREDENTIALS
    if not credentials:
        credentials = SF_CREDENTIALS
    # Start a connection
    connection = snowflake.connector.connect(
        account=credentials["account"],
        user=credentials["user"],
        password=credentials["password"],
    )

    # Create a cursor
    cursor = connection.cursor()

    if env != "PRODUCTION" and read == False:
        logger.info("Running Snowflake in development mode")
        credentials["database"] = "RAW_DEVELOPMENT"
        credentials["role"] = "LOADER"
        credentials["warehouse"] = "LOAD_WH"
    else:
        logger.info(
            "Running Snowflake in production mode or reading data from Snowflake"
        )

    # Set context
    if "role" in credentials.keys():
        cursor.execute(f"USE ROLE {credentials['role']}", _is_internal=True).fetchall()

    if "database" in credentials.keys():
        cursor.execute(
            f"USE DATABASE {credentials['database']}", _is_internal=True
        ).fetchall()

    if "schema" in credentials.keys():
        cursor.execute(
            f"USE SCHEMA {credentials['schema']}", _is_internal=True
        ).fetchall()

    if "warehouse" in credentials.keys():
        cursor.execute(
            f"USE WAREHOUSE {credentials['warehouse']}", _is_internal=True
        ).fetchall()

    return cursor


def engine_connect(
    database=None, schema=None, role=None, warehouse=None, credentials=None, read=False
):
    global SF_CREDENTIALS
    if not credentials:
        credentials = SF_CREDENTIALS
    engine = create_engine(
        "snowflake://{user}:{password}@{account}/".format(
            user=credentials["user"],
            password=credentials["password"],
            account=credentials["account"],
        )
    )

    connection = engine.connect()

    # Override the database in the credentials if passed
    if database:
        credentials["database"] = database

    # Override the schema in the credentials if passed
    if schema:
        credentials["schema"] = schema

    # Override the role in the credentials if passed
    if role:
        credentials["role"] = role

    # Override the warehouse in the credentials if passed
    if warehouse:
        credentials["warehouse"] = warehouse

    if env != "PRODUCTION" and read == False:
        logger.info("Running Snowflake in development mode")
        credentials["database"] = "RAW_DEVELOPMENT"
        credentials["role"] = "LOADER"
        credentials["warehouse"] = "LOAD_WH"
    else:
        logger.info(
            "Running Snowflake in production mode or reading data from Snowflake"
        )

    # Set context
    if "role" in credentials.keys():
        connection.execute(
            f"USE ROLE {credentials['role']}", _is_internal=True
        ).fetchall()

    if "database" in credentials.keys():
        connection.execute(
            f"USE DATABASE {credentials['database']}", _is_internal=True
        ).fetchall()

    if "schema" in credentials.keys():
        connection.execute(
            f"USE SCHEMA {credentials['schema']}", _is_internal=True
        ).fetchall()

    if "warehouse" in credentials.keys():
        connection.execute(
            f"USE WAREHOUSE {credentials['warehouse']}", _is_internal=True
        ).fetchall()

    return connection

def create_schema(schema):
    cursor = connect(SF_CREDENTIALS)
    cursor.execute(f'CREATE SCHEMA IF NOT EXISTS {schema}')
    

def query(sql_query, cursor=None, database=None, schema=None):
    global SF_CREDENTIALS
    if not cursor:

        # Validate inputs
        if database is not None and schema is None:
            raise ValueError("Schema has to be provided when a database is provided")

        # Connect to Snowflake
        if database is not None:
            SF_CREDENTIALS["database"] = database
            SF_CREDENTIALS["schema"] = schema

        cursor = connect(SF_CREDENTIALS)

    logger.debug(f"Running query: '{sql_query}'")
    results = cursor.execute(sql_query, _is_internal=True).fetchall()
    cursor.close()

    return results


def query_df(sql_query, conn):

    df = pd.read_sql(sql_query, conn)

    return df


def get_chunksize(shape):
    """
    NotImplemented: Returns the number of rows as the chunk size
    Return the number of chunks recommended for upload according to a shape tuple
    """

    rows, columns = shape
    return rows


def create_s3_stage(
    aws_key_id : str,
    aws_secret_key: str,
    s3_bucket_url: str,
    stage_name : str = None,
    temporary: bool = False) -> str:

    global SF_CREDENTIALS

    cursor = connect(SF_CREDENTIALS)
    temporary = ''
    if temporary:
        temporary = 'temporary'

    create_stage_sql = f"""
        /* Python:snowflake_utils */
        create or replace {temporary} stage "{stage_name}" 
        url = {s3_bucket_url} 
        credentials = (aws_secret_key = '{aws_secret_key}' aws_key_id = '{aws_key_id}')
        """

    logger.info(f"creating stage with '{create_stage_sql}'")
    cursor.execute(create_stage_sql, _is_internal=True).fetchall()
    return stage_name


def create_stage(
    cursor=None, database=None, schema=None, stage_name=None, temporary=True,
):
    """
    Create a stage in Snowflake
    If a name is not provided, a random name will be used
    """
    global SF_CREDENTIALS

    if not cursor:

        # Validate inputs
        if database is not None and schema is None:
            raise ValueError("Schema has to be provided when a database is provided")

        # Connect to Snowflake
        if database is not None:
            SF_CREDENTIALS["database"] = database
            SF_CREDENTIALS["schema"] = schema

        cursor = connect(SF_CREDENTIALS)

    temporary_sql = ""
    if temporary:
        temporary_sql = "temporary"

    # If a name is provided, will use that one
    if stage_name:
        create_stage_sql = f"""
        /* Python:snowflake_utils */
        create {temporary_sql} stage if not exists "{stage_name}"
        """

        logger.debug(f"creating stage with '{create_stage_sql}'")
        cursor.execute(create_stage_sql, _is_internal=True).fetchall()
        return stage_name

    while True:
        try:
            stage_name = "".join(
                random.choice(string.ascii_lowercase) for _ in range(5)
            )

            create_stage_sql = f"""
            /* Python:snowflake */
            create {temporary_sql} stage "{stage_name}"
            """

            logger.debug(f"creating stage with '{create_stage_sql}'")
            cursor.execute(create_stage_sql, _is_internal=True).fetchall()
            break
        except ProgrammingError as pe:
            if pe.msg.endswith("already exists."):
                continue
            raise

    return stage_name


def create_file_format(
    name,
    cursor=None,
    database=None,
    schema=None,
    format_type="JSON",
    compression="GZIP",
    strip_outer_array=True,
    if_exists="skip",
):
    global SF_CREDENTIALS

    if format_type != "JSON":
        raise NotImplementedError("Only JSON file formats are supported at this point")

    # Check if_exists validity
    if_exists_accepted_values = ["replace", "skip"]
    if if_exists not in if_exists_accepted_values:
        raise ValueError(
            f"Unknown value '{if_exists}' for argument `if_exits`. Accepted values are: {', '.join(if_exists_accepted_values)}"
        )

    replace_sql = ""
    if if_exists == "replace":
        replace_sql = "OR REPLACE"

    not_exists_sql = ""
    if if_exists == "skip":
        not_exists_sql = "IF NOT EXISTS"

    outer_array = "FALSE"
    if strip_outer_array:
        outer_array = "TRUE"

    if not cursor:

        # Validate inputs
        if database is not None and schema is None:
            raise ValueError("Schema has to be provided when a database is provided")

        # Connect to Snowflake
        if database is not None:
            SF_CREDENTIALS["database"] = database
            SF_CREDENTIALS["schema"] = schema

        cursor = connect(SF_CREDENTIALS)

    create_file_format_sql = f"""
    CREATE {replace_sql} FILE FORMAT {not_exists_sql} {name}
    TYPE = '{format_type}' COMPRESSION = '{compression}' ENABLE_OCTAL = FALSE 
    ALLOW_DUPLICATE = FALSE STRIP_OUTER_ARRAY = {outer_array} STRIP_NULL_VALUES = FALSE IGNORE_UTF8_ERRORS = FALSE;
    """
    logger.debug(f"creating file format with '{create_file_format_sql}'")
    result = cursor.execute(create_file_format_sql, _is_internal=True).fetchall()

    return result


def to_stage(
    file_path,
    stage_name,
    cursor=None,
    database=None,
    schema=None,
    parallel=4,
    purge=True,
):
    """
    Upload a file to an already existing Snowflake stage
    """

    global SF_CREDENTIALS
    if not cursor:

        # Validate inputs
        if database is not None and schema is None:
            raise ValueError("Schema has to be provided when a database is provided")

        # Connect to Snowflake
        if database is not None:
            SF_CREDENTIALS["database"] = database
            SF_CREDENTIALS["schema"] = schema

        cursor = connect(SF_CREDENTIALS)

    upload_sql = f"""
    /* Python:snowflake */
    PUT 'file://{file_path}' @"{stage_name}" PARALLEL={parallel}
    """

    logger.debug(f"Uploading files with '{upload_sql}'")
    cursor.execute(upload_sql, _is_internal=True)

    if purge:
        # Remove chunk file
        os.remove(file_path)


def restore_credentials():
    global SF_CREDENTIALS
    SF_CREDENTIALS = {
        "account": v.get("SF_ACCOUNT"),
        "user": v.get("SF_USER"),
        "password": v.get("SF_PASSWORD"),
        "role": v.get("SF_ROLE"),
        "database": v.get("SF_DATABASE", "RAW"),
        "warehouse": v.get("SF_WAREHOUSE", "LOAD_WH"),
    }

def df_to_snowflake(df, database, schema, table, if_exists='append'):

    df.columns = df.columns.str.upper()

    variant_type = {}
    has_variants = False
    row = df.iloc[0]
    for col in df.columns:
        if isinstance(row[col], dict) or isinstance(row[col], list):
            df[col] = pd.Series([json.dumps(x) for x in df[col]])
            has_variants = True
            variant_type[col] = True
        else:
            variant_type[col] = False

    with engine_connect(database=database.upper(), schema=schema.upper()) as conn:
        with conn.begin():

            df.to_sql(
                name=table, 
                con=conn, 
                index=False, 
                method=pd_writer, 
                if_exists=if_exists)

    if has_variants:
        sql = []
        for k,v in variant_type.items():
            if v:
                sql.append(f"parse_json({k}) {k}")
            else:
                sql.append(f"{k}")
        sql = ",\n".join(sql)
        sql = f"""
            create or replace table {database}.{schema}.{table} 
            as select {sql} from {database}.{schema}.{table}"""
        query(sql, database=database, schema=schema)


def load_df(
    df, table_name, database, schema, conn=None, if_exists="replace", datetime_type="ms"
):

    for column in df:
        if is_datetime(df[column]):
            df[column] = df[column].dt.ceil(freq=datetime_type)

    # Upper case all column names
    df.columns = map(str.upper, df.columns)

    with engine_connect(database=database.upper(), schema=schema.upper()) as conn:
        with conn.begin():
            df.to_sql(
                name=table_name.lower(),
                con=conn,
                if_exists=if_exists,
                method=pd_writer,
                index=False,
            )


def copy_stage(
    stage_name,
    table,
    cursor=None,
    database=None,
    schema=None,
    on_error="abort_statement",
    if_exists="replace",
    compression="GZIP",
    add_meta=False,
    file_format=None,
):

    global SF_CREDENTIALS
    if not cursor:
        # Validate inputs
        if database is not None and schema is None:
            raise ValueError("Schema has to be provided when a database is provided")

        # Connect to Snowflake
        if database is not None:
            SF_CREDENTIALS["database"] = database
            SF_CREDENTIALS["schema"] = schema

        cursor = connect(SF_CREDENTIALS)

    # Generate a location from the DB, schema, and table
    # "DATABASE"."SCHEMA"."TABLE"
    location = (
        (('"' + database + '".') if database else "")
        + (('"' + schema + '".') if schema else "")
        + ('"' + table + '"')
    )

    # Create file format if it doesn't exist
    default_file_format_name = "JSON_ARRAY_FILE_FORMAT"
    if not file_format:
        create_file_format(
            default_file_format_name,
            cursor=cursor,
            if_exists="skip",
            format_type="JSON",
            compression="GZIP",
            strip_outer_array=True,
        )
        file_format = default_file_format_name

    # Get compression
    if compression != "GZIP":
        raise NotImplementedError(
            f"Compression method '{compression}' not available. Currently only 'GZIP' is implemented"
        )

    # Extra columns for metadata
    meta_create_sql = ""
    meta_insert_sql = ""
    meta_select_sql = ""
    if add_meta:
        meta_create_sql = f"""
        , "INSERTED_AT" TIMESTAMP
        , "FILE_NAME" VARCHAR
        , "FILE_ROW" VARCHAR
        """
        meta_insert_sql = f"""
        , "INSERTED_AT"
        , "FILE_NAME"
        , "FILE_ROW"
        """
        meta_select_sql = f"""
        , current_timestamp
        , metadata$filename
        , metadata$file_row_number
        """

    # Check if_exists validity
    if_exists_accepted_values = ["replace", "append"]
    if if_exists not in if_exists_accepted_values:
        raise ValueError(
            f"Unknown value '{if_exists}' for argument `if_exits`. Accepted values are: {', '.join(if_exists_accepted_values)}"
        )

    # Create table
    if if_exists == "replace":
        # Create or replace the table
        create_table_sql = f"""
        /* Python:snowflake */
        create or replace table {location} ("JSON_DATA" VARIANT {meta_create_sql})
        """
        logger.debug(f"creating table with '{create_table_sql}'")

        cursor.execute(create_table_sql, _is_internal=True).fetchall()

    if if_exists == "append":
        # Combination of append and meta warning
        if add_meta:
            logger.warning(
                "Using 'append' method in combination with 'add_meta' can generate conflicts if the table does not have meta columns already"
            )

        # Create table if it does not exist
        create_table_sql = f"""
        /* Python:snowflake */
        create table if not exists {location} ("JSON_DATA" VARIANT {meta_create_sql})
        """
        logger.debug(f"creating table with '{create_table_sql}'")

        cursor.execute(create_table_sql, _is_internal=True).fetchall()

    copy_into_sql = f"""
    /* Python:snowflake */
    COPY INTO {location} ("JSON_DATA" {meta_insert_sql})
    FROM (
        SELECT t.*
        {meta_select_sql}
        FROM @"{stage_name}" (FILE_FORMAT => {file_format}) t
    )
    FORCE=TRUE PURGE=TRUE ON_ERROR={on_error}
    """
    logger.debug(f"copying into with '{copy_into_sql}'")
    copy_results = cursor.execute(copy_into_sql, _is_internal=True).fetchall()
    cursor.close()

    try:
        return (
            all((e[1] == "LOADED" for e in copy_results)),
            len(copy_results),
            sum((e[3] for e in copy_results)),
            copy_results,
        )
    except IndexError:
        # There was an index error while getting the response
        # But there was no error executing the query
        # Must be that we didn't insert anything
        # [('Copy executed with 0 files processed.',)]
        logger.warning("IndexError was raised. Most likely an empty insertion.")
        return (True, 0, 0, copy_results)


def list_writer(
    data,
    table,
    cursor=None,
    schema=None,
    database=None,
    if_exists="replace",
    file_prefix="file",
    chunksize=None,
    add_meta=False,
):

    global SF_CREDENTIALS
    if not cursor:
        # Validate inputs
        if database is not None and schema is None:
            raise ValueError(
                "Schema has to be provided to list_writer when a database is provided"
            )

        # Connect to Snowflake
        if database is not None:
            SF_CREDENTIALS["database"] = database
            SF_CREDENTIALS["schema"] = schema

        cursor = connect(SF_CREDENTIALS)

    # Will store generated file names here
    file_names = []

    # Create a temporary stage in Snowflake
    stage_name = create_stage(cursor=cursor)

    # If no chunks provided, use recommended
    if chunksize is None:
        # chunksize = get_chunksize(df.shape)
        # TODO: Figure out how to get chunksize
        chunksize = len(data)

    # Split list into chunksize
    chunks = [data[i : (i + chunksize)] for i in range(0, len(data), chunksize)]

    # Iterate the chunks
    for idx, chunk in enumerate(chunks):

        # Generate a file name
        file_name = f"{file_prefix}_{idx}.json.gz"
        file_names.append(file_name)

        # Write list to JSON
        json_str = json.dumps(chunk, default=str)
        json_bytes = json_str.encode("utf-8")
        with gzip.GzipFile(file_name, "w") as file_out:
            file_out.write(json_bytes)

        # Upload chunk to stage
        to_stage(file_name, stage_name, cursor=cursor)

    # Copy into table from stage
    response = copy_stage(
        stage_name, table, cursor=cursor, add_meta=add_meta, if_exists=if_exists
    )
    success, chunks, rows, output = response

    # Log results
    logger.debug(f"Success: {success}")
    logger.debug(f"Chunks: {chunks}")
    logger.debug(f"Rows: {rows}")
    logger.debug(f"Output: {output}")

    # Check results
    if success:
        logger.debug(f"Loaded {chunks} files in Snowflake inserting {rows} rows")
    else:
        logger.debug("Error loading files to Snowflake")
        logger.debug(response)
        raise RuntimeError("failed to load all files to Snowflake")

    return response


def list_stager(
    data,
    stage_name,
    cursor=None,
    database=None,
    schema=None,
    file_prefix="file",
    chunksize=None,
    dumper=json.dumps,
):
    global SF_CREDENTIALS
    if not cursor:
        # Validate inputs
        if database is not None and schema is None:
            raise ValueError(
                "Schema has to be provided to list_writer when a database is provided"
            )

        # Connect to Snowflake
        if database is not None:
            SF_CREDENTIALS["database"] = database
            SF_CREDENTIALS["schema"] = schema

        cursor = connect(SF_CREDENTIALS)

    # Will store generated file names here
    file_names = []

    # Create a stage in Snowflake (non-temporary)
    stage_name = create_stage(cursor=cursor, stage_name=stage_name, temporary=False)

    # If no chunks provided, use recommended
    if chunksize is None:
        # chunksize = get_chunksize(df.shape)
        # TODO: Figure out how to get chunksize
        chunksize = len(data)

    # Split list into chunksize
    chunks = [data[i : (i + chunksize)] for i in range(0, len(data), chunksize)]

    # Iterate the chunks
    for idx, chunk in enumerate(chunks):

        # Generate a file name
        file_name = f"{file_prefix}_{idx}.json.gz"
        file_names.append(file_name)

        # Write list to JSON
        json_str = dumper(chunk)
        json_bytes = json_str.encode("utf-8")
        with gzip.GzipFile(file_name, "w") as file_out:
            file_out.write(json_bytes)

        # Upload chunk to stage
        to_stage(file_name, stage_name, cursor=cursor)


def drop_clone(db_clone_name):
    credentials = {
        "account": v.get("SF_ACCOUNT"),
        "user": v.get("SF_USER"),
        "password": v.get("SF_PASSWORD"),
        "role": "TRANSFORMER",
    }

    if db_clone_name in ["ANALYTICS", "STAGING", "RAW"]:
        logger.warning(
            f"Drop action denied! Trying to drop {db_clone_name} database in Snowflake is prohibited."
        )
        raise ValueError(f"Tried to drop prohibited database '{db_clone_name}'")

    else:
        cursor = connect(credentials=credentials, read=True)
        cursor.execute(f"DROP DATABASE IF EXISTS {db_clone_name}")
