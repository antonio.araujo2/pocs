import json

import requests
from requests.auth import HTTPBasicAuth


class API_rest:

    def get_basic_auth(self, endpoint, user, key, params):

        # Authentication in Requests with HTTPBasicAuth
        auth = HTTPBasicAuth(
            user, 
            key)
        
        return requests.get(endpoint, params=params, auth=auth)

    def get_bearer(self, 
                    endpoint, 
                    bearer_token, 
                    payload: dict = None, 
                    content_type: dict = None):

        # Authentication via bearer token
        header = {'Authorization': 'Bearer ' + bearer_token}

        if content_type:
            header = {**header, **content_type}
        
        payload = json.dumps(payload)
        return requests.get(endpoint, headers=header, data=payload)

    def post_basic_auth(self, endpoint, user, key, params=None):

        # Authentication in Requests with HTTPBasicAuth
        auth = HTTPBasicAuth(
            user, 
            key)
        
        return requests.post(endpoint, json=params, auth=auth)
