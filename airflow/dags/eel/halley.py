import os

import pandas as pd
import sqlalchemy as db
# Import logger
from airflow.models import Variable as v
from eel.logger import Logger

# Initialize logger
log = Logger("eel")
log.set_function("halley")

# Database credentials
DB_AUTH = {
    "db_user": v.get("HALLEY_USER"),
    "db_password": v.get("HALLEY_PASSWORD"),
    "db_host_name": v.get("HALLEY_HOST"),
    "db_port": v.get("HALLEY_PORT"),
    "db_name": v.get("HALLEY_DB"),
}


class Halley:
    def __init__(self, auth=DB_AUTH):
        # Get credentials
        log.debug("Setting DB credentials")
        self.db_user = DB_AUTH["db_user"]
        self.db_password = DB_AUTH["db_password"]
        self.db_host_name = DB_AUTH["db_host_name"]
        self.db_port = DB_AUTH["db_port"]
        self.db_name = DB_AUTH["db_name"]

        # Generate connection URL
        log.debug("Generate connection URL")
        self.db_auth_str = self.db_user
        if self.db_password:
            self.db_auth_str += ":" + self.db_password
        self.db_url = f"postgresql://{self.db_auth_str}@{self.db_host_name}:{self.db_port}/{self.db_name}"

    def __str__(self):
        if self._check_credentials():
            return f"<Halley {self.db_host_name}/{self.db_name}>"
        else:
            return f"<Halley NoCredentialsProvided>"

    def __repr__(self):
        if self._check_credentials():
            return f"<Halley {self.db_host_name}/{self.db_name}>"
        else:
            return f"<Halley NoCredentialsProvided>"

    def _check_credentials(self):
        if "None" in self.db_url:
            return False
        else:
            return True

    def _connect(self):
        # Connecting to PostgreSQL
        log.debug("Connecting to Halley")
        engine = db.create_engine(self.db_url)
        return engine

    def execute(self, query):
        log.info(f"Execute query: {query}")
        if not self._check_credentials():
            raise ConnectionError("No credentials provided")
        engine = self._connect()
        with engine.connect() as con:
            response = con.execute(query)
        return response

    def query(self, query):
        if not self._check_credentials():
            raise ConnectionError("No credentials provided")
        engine = self._connect()
        df = pd.read_sql(query, engine)
        return df

    def write(
        self, df, table_name, schema, if_exists="replace", chunksize=10000, index=True
    ):
        engine = self._connect()
        df.to_sql(
            table_name,
            engine,
            schema=schema,
            if_exists=if_exists,
            chunksize=10000,
            index=index,
        )
        return True
