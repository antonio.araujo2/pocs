import os
import re

import eel as eel
import gspread
import pandas as pd
from airflow.models import Variable as v
from eel import helpers as helper
from eel.halley import Halley
from eel.logger import Logger
from oauth2client.service_account import ServiceAccountCredentials

log = Logger("gsheets")

def get_google_auth_credentials(company: str = 'sirena'):
    company = company.upper()
    return {
        "type": v.get('GOOGLE_AUTH_TYPE'),
        "project_id": v.get(f'GOOGLE_AUTH_{company}_PROJECT'),
        "private_key_id": v.get(f'GOOGLE_AUTH_{company}_PRIVATE_KEY_ID'),
        "private_key": v.get(f'GOOGLE_AUTH_{company}_PRIVATE_KEY').replace('\\n', '\n'),
        "client_email": v.get(f'GOOGLE_AUTH_{company}_EMAIL'),
        "client_id": v.get(f'GOOGLE_AUTH_{company}_CLIENT_ID'),
        "auth_uri": v.get(f'GOOGLE_AUTH_URI'),
        "token_uri": v.get('GOOGLE_AUTH_TOKEN_URI'),
        "auth_provider_x509_cert_url": v.get(f'GOOGLE_AUTH_{company}_PROVIDER_CERT_URL'),
        "client_x509_cert_url": v.get(f'GOOGLE_AUTH_{company}_CLIENT_CERT_URL')
    }

DEFAULT_SCHEMA = 'googlesheets'


def get_sheet(worksheet_name, 
              spreadsheet_key=None, 
              spreadsheet_url=None, 
              spreadsheet_name=None, 
              headers_row=1,
              company='sirena'):
    """ Open a Google Sheet and return content in a DataFrame

    This function imports all the data from a Google Sheets file from a given
    sheet and creates a Pandas' DataFrame with it.

    Args:
        spreadsheet_key (str): The key of the Google Sheets spreadsheet
        spreadsheet_url (str): The URL of the Google Sheets spreadsheet
        spreadsheet_name (str): The name of the Google Sheets spreadsheet
        worksheet_name (str): The name of the worksheet inside the spreadsheet to import data

    Returns:
        DataFrame: The data inside a Pandas' DataFrame

    """

    # Initialize logging
    log.set_function("get_sheet")
    log.debug(f"worksheet_name: {worksheet_name}")
    log.debug(f"spreadsheet_name: {spreadsheet_name}")
    log.debug(f"spreadsheet_key: {spreadsheet_key}")
    log.debug(f"spreadsheet_url: {spreadsheet_url}")
    log.debug(f"headers_row: {headers_row}")

    # Check arguments types
    log.info("Checking arguments")
    if (not isinstance(spreadsheet_name, str)) and spreadsheet_name != None:
        log.error(f"file_name: {spreadsheet_name}")
        raise TypeError("Expected argument 'spreadsheet_name' to be of type 'str'")
    if (not isinstance(spreadsheet_key, str)) and spreadsheet_key != None:
        log.error(f"file_name: {spreadsheet_key}")
        raise TypeError("Expected argument 'spreadsheet_key' to be of type 'str'")
    if (not isinstance(spreadsheet_url, str)) and spreadsheet_url != None:
        log.error(f"file_name: {spreadsheet_url}")
        raise TypeError("Expected argument 'spreadsheet_url' to be of type 'str'")
    if not isinstance(worksheet_name, str):
        log.error(f"worksheet_name: {worksheet_name}")
        raise TypeError("Expected argument 'worksheet_name' to be of type 'str'")

    # Use credentials to create a client to interact with the Google Drive API
    log.info("Authenticating Google Sheets")
    scope = ['https://www.googleapis.com/auth/drive',
             'https://www.googleapis.com/auth/spreadsheets.readonly',
             'https://www.googleapis.com/auth/spreadsheets']
    creds = ServiceAccountCredentials.from_json_keyfile_dict(
        get_google_auth_credentials(company=company), scope)
    client = gspread.authorize(creds)

    # Find a spreadsheet by name and open the sheet
    log.info("Opening spreadsheet")

    if(spreadsheet_key != None):
        sheet = client.open_by_key(spreadsheet_key).worksheet(worksheet_name)
    elif(spreadsheet_url != None):
        sheet = client.open_by_url(spreadsheet_url).worksheet(worksheet_name)
    elif(spreadsheet_name != None):
        sheet = client.open(spreadsheet_name).worksheet(worksheet_name)
    else:
        return None

    # Extract all of the values
    log.info("Get all values")
    list_of_hashes = sheet.get_all_records(head=headers_row)

    # Put data into a Pandas DataFrame
    log.info("Import data into DataFrame")
    df = pd.DataFrame(list_of_hashes)
    log.debug(f"Imported columns: {df.columns.to_list()}")
    log.debug(f"Imported shape: {df.shape}")

    return df


def clean_data(data, mapper, drop_duplicates=False, skip_index=False):
    """ Given a data mapper and a DataFrame perform a series of cleaning procedures

    Uses the mapper to clean and validate the data:
        1. Check absent columns
        2. Check extra columns
        3. Renames columns
        4. Parse types
        5. Checks for duplicates
        6. Sets index

    Args:
        data (DataFrame): The DataFrame with all the data
        mapper (dict): The dictionary mapper with all column names, aliases and types

    Returns:
        DataFrame: A clean version of the initial DataFrame

    """

    # Initialize logging
    log.set_function("clean_data")
    log.debug(f"mapper: {mapper}")
    log.debug(f"drop_duplicates: {drop_duplicates}")
    log.debug(f"Data shape: {data.shape}")

    # Clean data
    df = helper.clean_data(
        data, mapper, drop_duplicates=drop_duplicates, skip_index=skip_index)

    log.debug(f"Result shape: {df.shape}")

    return df


def send_to_db(data, db_table_name, db_schema=DEFAULT_SCHEMA):

    # Initialize logging
    log.set_function("send_to_db")
    log.debug(f"db_table_name: {db_table_name}")
    log.debug(f"db_schema: {db_schema}")

    # Write to the database
    log.info("Writing to DB")
    Halley().write(data, db_table_name, db_schema)

    return True
