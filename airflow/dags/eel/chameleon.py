import os
import time

import pandas as pd
import requests
from airflow.models import Variable as v


class Chameleon:
    def __init__(self, key=None):
        if key:
            self.key = key
        else:
            self.key = v.get('CHAMELEON_KEY')
        self.url = 'https://api.trychameleon.com/v3/'

    def get(self, endpoint, data_name, extra_params=None):

        raw_data = list()

        response = self.__get_chameleon_data(
            endpoint,
            params=self.__merge_two_dicts({'limit': 500}, extra_params))

        if isinstance(response.json()[data_name], dict):
            # We need to insert a list even if it have a single item
            aux = list()
            aux.append(response.json()[data_name])
            raw_data.extend(aux)
        # is a list
        else:
            raw_data.extend(response.json()[data_name])

        # Cursor is not None
        if "cursor" in response.json():
            cursor = response.json()['cursor']['before']
            while cursor is not None:
                response = self.__get_chameleon_data(
                    endpoint,
                    params=self.__merge_two_dicts(
                        {'limit': 500, 'before': cursor}, extra_params))
                raw_data.extend(response.json()[data_name])
                cursor = response.json()['cursor']['before']
                time.sleep(1)

        return raw_data

    def __get_chameleon_data(self, path, params):
        params['account_secret'] = self.key
        response = requests.get(self.url + path, params=params)
        response.raise_for_status()
        return response

    def __merge_two_dicts(self, x, y):
        if y == None:
            return x
        else:
            z = x.copy()   # start with keys and values of x
            z.update(y)    # modifies z with keys and values of y
            return z
