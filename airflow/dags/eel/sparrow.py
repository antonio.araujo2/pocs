import os
import time

import pandas as pd
import requests
from airflow.models import Variable as v

# date.gte -->  Greater than filter
# date.lte -->  Less than Filter
# Date Format (MM/DD/YYYY)

SPARROW_TOKENS = {
    "sirena": v.get("SPARROW_SIRENA_TOKEN"),
    "zenvia": v.get("SPARROW_ZENVIA_TOKEN"),
}

class BearerAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token
    def __call__(self, r):
        r.headers['authorization'] = 'Bearer ' + self.token
        return r

class Sparrow:
    def __init__(self, company: str): 
        self.key = SPARROW_TOKENS[company]
        self.url = 'https://api.surveysparrow.com/v1/'

    def get(self, endpoint, G_id, nps_id, pagination, from_date='01/01/2021', to_date='01/01/2022'):
        raw_data = list()
        ids = list()
        if G_id:
            ids = self.__get_ids()
        elif nps_id:
            ids = self.__get_nps_ids()

        if endpoint == 'surveys': # Get Surveys
            page_nro = 1
            while True:
                response = self.__get_sparrow_data('surveys', params={'page': page_nro})
                has_next_page = response.json()['hasNextPage']
                raw_data.extend(response.json()[endpoint])
                if has_next_page:
                    page_nro += 1
                    time.sleep(1)
                else:
                    break
        elif endpoint == 'submissions': # Get Submissions
            for id in ids:
                params = {'date.gte': from_date,'date.lte': to_date}
                response = self.__get_sparrow_data(
                    'surveys/'+str(id)+'/submissions',
                    params=params).json()[endpoint]
                raw_data.extend(response)
                time.sleep(1)
        elif endpoint == 'responses': # Get NPS Responses
            for id in ids:
                raw_data.extend(self.__get_sparrow_data('nps/'+str(id)+'/responses',params={'date.gte': from_date,'date.lte': to_date}).json())
        else: # Generic Get FINISH!
            # if len(ids) == 0:
            #     ids.append(1)
            # params = dict()
            # for id in ids:
            #     page_nro = 1
            #     has_next_page = False
            #     while True:
            #         if G_id or nps_id:
            #             path = startpoint+'/'+str(id)+'/'+endpoint
            #         else: 
            #             path = startpoint
            #         response = self.__get_sparrow_data(path, params=params)
            #         if pagination:
            #             has_next_page = response.json()['hasNextPage']
            #         raw_data.extend(response.json()[endpoint])
            #         if has_next_page:
            #             page_nro += 1 
            #         else:
            #             break
            pass
        return raw_data

    def __get_sparrow_data(self, path, params=None):
        if params is None:
            response = requests.get(self.url + path,
                auth=BearerAuth(self.key))
        else:
            response = requests.get(self.url + path,
                auth=BearerAuth(self.key),
                params=params)    
        response.raise_for_status()
        time.sleep(1)
        return response


    def __get_ids(self):
        ids = list()
        page_nro = 1
        while True:
            response = self.__get_sparrow_data('surveys', params={'page': page_nro})
            has_next_page = response.json()['hasNextPage']
            raw_data = response.json()['surveys']
            ids.extend(pd.DataFrame.from_dict(raw_data)['id'])
            if has_next_page:
                page_nro += 1 
            else:
                break
        return ids


    def __get_nps_ids(self):
        ids = list()
        page_nro = 1
        while True:
            response = self.__get_sparrow_data('surveys', params={'page': page_nro})
            has_next_page = response.json()['hasNextPage']
            raw_data = response.json()['surveys']
            df = pd.DataFrame.from_dict(raw_data)
            df = df.drop(df[df.surveyType != 'NPS'].index)
            ids.extend(df['id'])
            if has_next_page:
                page_nro += 1 
            else:
                break
        return ids
