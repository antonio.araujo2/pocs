import re
from datetime import datetime, timedelta

import pandas as pd
# Import logger
from eel.logger import Logger

# Initialize logger
log = Logger("eel")
log.set_function("helpers")


def day_iterator(day: datetime.date, hours: int = 1):
    """
    Iterates through a day with a given step in hours.
    """

    initial_dt = datetime.combine(day, datetime.min.time())
    final_dt = initial_dt + timedelta(days=1)

    current_dt = initial_dt

    while current_dt < final_dt:
        next_dt = current_dt + timedelta(hours=hours)

        if next_dt > final_dt:
            next_dt = final_dt

        yield (current_dt, next_dt)
        current_dt = current_dt + timedelta(hours=hours)


def clean_column_name(col_name):
    col_name = col_name.strip()
    col_name = "_".join(a.lower() for a in col_name.split(" ") if a.isalnum())
    return col_name


def get_mapper(df):
    columns = df.columns.to_list()
    suggested_mapper = {}
    for col in columns:
        clean_name = clean_column_name(col)
        suggested_mapper[col] = {"name": clean_name, "dtype": "text"}
    return suggested_mapper


def clean_data(df, mapper, drop_duplicates=False, skip_index=False):
    # Process the data mapper
    log.debug("Processing the mapper")
    col_mapper = {}
    col_parser = {}
    index_col = []
    for col in mapper:
        col_mapper[col] = mapper[col]["name"]
        cast = False
        dtype = "text"
        if "cast" in mapper[col].keys():
            cast = mapper[col]["cast"]
        if "dtype" in mapper[col].keys():
            dtype = mapper[col]["dtype"]
        if "index" in mapper[col].keys():
            index_col.append(mapper[col]["name"])
        if cast and dtype != "text":
            col_parser[mapper[col]["name"]] = mapper[col]["dtype"]
    log.debug(f"col_mapper: {col_mapper}")
    log.debug(f"col_parser: {col_parser}")
    log.debug(f"index_col: {index_col}")

    if not skip_index:
        # We need at least one column that is an index
        log.debug("Check if index is present")
        assert len(index_col) > 0, "ERROR: No index column specified in mapper."
        index_col = index_col[0]
        log.debug(f"index_col: {index_col}")

    # Rename columns
    log.debug("Rename columns")
    df.rename(columns=col_mapper, inplace=True)

    # Get columns not in the mapper
    # If the column is missing from the mapper show a warning and drop the columns
    # If a column is in the mapper and not found show warning
    no_int = list(set(df.columns) ^ set(col_mapper.values()))
    drop_cols = []
    missing_cols = []

    for col in no_int:
        columns = df.columns.to_list()
        if col in columns:
            drop_cols.append(col)
        else:
            missing_cols.append(col)

    log.debug("Check missing columns")

    if len(drop_cols) > 0:
        log.warning(f"Missing column definition for column(s): {', '.join(drop_cols)}")
        df.drop(columns=drop_cols, inplace=True)

    if len(missing_cols) > 0:
        log.warning(f"Expected column(s) not found: {', '.join(missing_cols)}")

    # Parse column types
    log.debug("Parse column types")
    for col in col_parser:
        dtype = col_parser[col]
        if dtype == "numeric":
            df[col] = pd.to_numeric(df[col], errors="coerce")
        if dtype == "double":
            df[col] = pd.to_numeric(df[col], errors="coerce")
        if dtype == "bool" or dtype == "boolean":
            df[col] = df[col].apply(lambda x: x.lower() == "true")
        if dtype == "datetime":
            # https://github.com/snowflakedb/snowflake-connector-python/issues/600
            df[col] = pd.to_datetime(df[col], errors="coerce", utc=True)
        else:
            log.warning(f"Unknown dtype '{dtype}'")

    # Force all object columns to be strings
    # For some reason, when storing df as Parquet (for Snowflake upload)
    # Type is determined by the values in the column instead of the pandas dtype
    # If you have an object column with values: 5, 6, 7, 8 it should be stored as string but it infers it as int
    # The problem arises when your column has values: 5, 6, 7, "8", this throws a pyarrow TypeError
    # With this code we ensure all object columns are stored as string (expected behavior)
    log.debug("Force all object columns to be strings")
    object_columns = df.select_dtypes(include="object").columns

    for column in object_columns:
        df[column] = df[column].astype(str)

    if drop_duplicates:
        # Remove duplicates
        log.debug("Removing duplicates")
        df.drop_duplicates(subset=[index_col], keep="first", inplace=True)
    else:
        if not skip_index:
            # Check that all ids are unique
            log.debug("Check that index is unique")
            assert len(df[index_col].unique()) == len(
                df[index_col]
            ), "ERROR: Duplicate records found."

    if not skip_index:
        # Set the index in the DataFrame
        log.debug("Set index")
        df.set_index(index_col, inplace=True)

    return df
