import json
import os
from datetime import datetime

import requests
from airflow.models import Variable as v

WEBHOOKS = {
    "#retention-alerts": "https://hooks.slack.com/services/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",
    "#test": "https://hooks.slack.com/services/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",
}

SLACK_ACCESS_TOKEN = v.get("SLACK_ACCESS_TOKEN")
SLACK_HISTORY_ENDPOINT = "https://slack.com/api/conversations.history"


class Blocks:
    def __init__(self):
        self.blocks = []
        self.attachments = []

    def send(self, channel, username=None, icon_url=None, icon_emoji=None):
        message = {"blocks": self.blocks}

        headers = {"Content-type": "application/json"}

        try:
            url = WEBHOOKS[channel]
        except:
            url = "https://slack.com/api/chat.postMessage"
            message["channel"] = channel
            print(SLACK_ACCESS_TOKEN)
            headers["Authorization"] = "Bearer {}".format(SLACK_ACCESS_TOKEN)

        if username:
            message["username"] = username

        try:
            url = WEBHOOKS[channel]
        except:
            url = "https://slack.com/api/chat.postMessage"
            message["channel"] = channel
            headers["Authorization"] = f"Bearer {SLACK_ACCESS_TOKEN}"

        if username:
            message["username"] = username

        if icon_url:
            message["icon_url"] = icon_url

        if icon_emoji:
            message["icon_emoji"] = icon_emoji

        payload = json.dumps(message)

        response = requests.request("POST", url, headers=headers, data=payload)

        return response

    def send_attachments(self, channel, username=None, icon_url=None, icon_emoji=None):
        message = {"attachments": self.attachments}

        headers = {"Content-Type": "application/json"}

        try:
            url = WEBHOOKS[channel]
        except:
            url = "https://slack.com/api/chat.postMessage"
            message["channel"] = channel
            headers["Authorization"] = f"Bearer {SLACK_ACCESS_TOKEN}"

        if username:
            message["username"] = username

        if icon_url:
            message["icon_url"] = icon_url

        if icon_emoji:
            message["icon_emoji"] = icon_emoji

        payload = json.dumps(message)

        response = requests.request("POST", url, headers=headers, data=payload)

        return response

    def add_attachment(self, color=None, pretext=None, author_name=None,
                       author_link=None, author_icon=None, title=None, title_link=None,
                       text=None, fields=None, thumb_url=None, footer=None, footer_icon=None):
        ts = datetime.now().timestamp()
        attachment = {
            "mrkdwn_in": ["text"],
            "color": color,
            "pretext": pretext,
            "author_name": author_name,
            "author_link": author_link,
            "author_icon": author_icon,
            "title": title,
            "title_link": title_link,
            "text": text,
            "fields": fields,
            "thumb_url": thumb_url,
            "footer": footer,
            "footer_icon": footer_icon,
            "ts": ts
        }
        current_attachemnts = self.attachments
        current_attachemnts.append(attachment)
        self.attachemnts = current_attachemnts

    def add_block(self, new_block):
        current_blocks = self.blocks
        current_blocks.append(new_block)
        self.blocks = current_blocks

    def add_section(self, text):
        section = {"type": "section", "text": {"type": "mrkdwn", "text": text}}
        self.add_block(section)

    def add_context(self, text):
        context = {"type": "context", "elements": [{"text": text, "type": "mrkdwn"}]}
        self.add_block(context)

    def add_divider(self):
        self.add_block({"type": "divider"})
