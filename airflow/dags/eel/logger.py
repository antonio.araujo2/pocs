import os

from airflow.models import Variable as v

LOG_LEVELS = {
    "DEBUG": 10,
    "INFO": 20,
    "WARNING": 30,
    "ERROR": 40,
    "CRITICAL": 50,
}

MAX_LOG = v.get("MAX_LOG") or "DEBUG"


class Logger:

    def __init__(self, file, function=None, max_log=MAX_LOG):
        self.file = file
        self.function = function
        self.max_log = max_log

    def _print(self, message, level="INFO"):
        function_message = ""
        if self.function:
            function_message = f"[{self.function}]"

        if LOG_LEVELS[level] >= LOG_LEVELS[self.max_log]:
            print(f"{level}: [{self.file}]{function_message} {message}")

    def set_file(self, file):
        self.file = file

    def set_function(self, function):
        self.function = function

    def set_max_log(self, level):
        self.max_log = level

    def log(self, message, level):
        self._print(message, level)

    def info(self, message):
        self._print(message)

    def debug(self, message):
        self._print(message, "DEBUG")

    def warning(self, message):
        self._print(message, "WARNING")

    def error(self, message):
        self._print(message, "ERROR")

    def critical(self, message):
        self._print(message, "CRITICAL")
