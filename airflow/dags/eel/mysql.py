from collections import OrderedDict

import mysql.connector
from mysql.connector import errorcode


class MySQL():
    """
        Python Class for connecting  with MySQL server and accelerate development project using MySQL
        Extremely easy to learn and use, friendly construction.
    """

    __instance   = None
    __host       = None
    __user       = None
    __password   = None
    __database   = None
    __session    = None
    __connection = None

    def __init__(self, 
                 host='localhost', user='root', password='', database=''):
        self.__host = host
        self.__user = user
        self.__password = password
        self.__database = database


    def open_connection(self):
        try:
            cnx = mysql.connector.connect(
                user=self.__user, 
                password=self.__password, 
                host=self.__host, 
                database=self.__database
            )

            self.__connection = cnx
            self.__session = cnx.cursor()

        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
            
    def get_cursor(self):
        if self.__session is None:
            self.open_connection()
        return self.__session

    def __close(self):
        self.__session.close()
        self.__connection.close()

    def select(self, table, where = None, star = True, chunk_size = 0, offset = 0, *args, **kwargs):

        result = None
        query = 'SELECT '

        if star:
            query += '* '            
        else:
            keys = args
            values = tuple(kwargs.values())
            l = len(keys) - 1

            for i, key in enumerate(keys):
                query += "`" + key.upper() + "`"
                if i < l:
                    query += ", "
            query += " "
            ## End for keys

        query += 'FROM ' + table

        if where:
            query += " WHERE " + where
            ## End if where
        
        if chunk_size > 0:
            query += f' LIMIT {chunk_size}'
        
        if offset > 0:
            query += f' OFFSET {offset}'
            
        print(f'Query: {query}')
        
        self.open_connection()
        
        if star:
            self.__session.execute(query)
        else:
            self.__session.execute(query, values)
        
        result = [item for item in self.__session.fetchall()]
            
        self.__close()

        return result

    def select_advanced(self, sql, *args):
        od = OrderedDict(args)
        query  = sql
        values = tuple(od.values())
        self.__open()
        self.__session.execute(query, values)
        number_rows = self.__session.rowcount
        number_columns = len(self.__session.description)

        if number_rows >= 1 and number_columns > 1:
            result = [item for item in self.__session.fetchall()]
        else:
            result = [item[0] for item in self.__session.fetchall()]

        self.__close()
        return result