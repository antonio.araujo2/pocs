from dataclasses import dataclass
from datetime import datetime, timedelta


@dataclass
class TableConfig:

    table_name: str
    primary_key: str
    date_fields: list
    catchup: bool
    start_date: datetime
    method: str
    interval_hours: int
    columns_to_string: list

    def __post__init(self) -> None:
        if self.method not in  ['full', 'append', 'incremental']:
            raise ValueError('O campo method precisa ter um dos valores a seguir: full, append ou incremental')
        if self.interval_hours < 1 or self.interval_hours > 24:
            raise ValueError('O campo interval_housrs deve ser um número entre 1 e 24')


def datetime_range(start, end, delta) -> tuple:

    current = start
    while current < end:
        yield current, current + delta
        current += delta
