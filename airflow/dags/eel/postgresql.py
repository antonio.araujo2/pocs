import os
import uuid

import pandas as pd
import psycopg2 as pg
# Import logger
from airflow.models import Variable as v
from eel.logger import Logger
from pandas import DataFrame
from sqlalchemy import create_engine

# Initialize logger
log = Logger("eel")
log.set_function("postgresql")

UUID_TYPE_CODE = 2950

class PostgreSQL:

    def __init__(self, database, user, host, port = 5432, password = None):

        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.database = database


    def __str__(self):
        return f'PostgreSQL - {self.host}: {self.database}'
    
    def _connect(self):
        uri = f"postgresql+psycopg2://{self.user}:{self.password}@{self.host}:{self.port}/{self.database}"

        return create_engine(uri)

    def execute(self, query):
        log.debug(f"Execute query: {query}")
        engine = self._connect()
        with engine.connect() as connection:
            result = connection.execute(query)
    
    def query(self, query):
        log.debug(f"Execute query: {query}")
        engine = self._connect()
        with engine.connect() as connection:
            result = connection.execute(query)
            df = DataFrame(result.fetchall())
            if len(df) > 0:
                df.columns = result.keys()
            return df

    def _fix_uuid_column(self, dataframe, coluna):

        dataframe[[coluna]] = dataframe[[coluna]].astype(str)
        return dataframe

    def query_chunk(self, table, query, chunksize):
        log.debug(f"Execute query: {query}")
        engine = self._connect()
        first_loop = True
        df = pd.DataFrame()

        loop_count = 1
        for dfx in pd.read_sql(query, engine, coerce_float=False, chunksize=chunksize):
            log.debug(f'Chunk {loop_count} : {len(dfx)} lines')
            loop_count += 1
            if first_loop:
                df = dfx
                first_loop = False
            else:
                df = pd.concat([df, dfx], ignore_index=True)

        colunas = self.get_columns_from_table(table)

        for coluna in colunas:
            if coluna.get('tipo') == UUID_TYPE_CODE:
                df = self._fix_uuid_column(dataframe=df, coluna=coluna.get('nome'))

        log.debug(colunas)

        return df

    def get_columns_from_table(self, table):
        query = f"select * from {table} limit 0"
        engine = self._connect()
        with engine.connect() as connection:
            result = connection.execute(query)
            return [
                {'nome': coluna[0],'tipo': coluna[1]} for coluna in list(result.cursor.description)]


    def select(
        self, table, where = None, star = True, limit: int = None, *args, **kwargs) -> pd.DataFrame:

        query = 'SELECT '

        if star:
            query += '* '
        else:
            keys = args
            l = len(keys) - 1

            for i, key in enumerate(keys):
                query +=  key
                if i < l:
                    query += ","

        query += 'FROM ' + table

        if where:
            query += " WHERE " + where
        ## End if where
        if limit:
            query += f" LIMIT {limit}"

        return self.query_chunk(table=table, query=query, chunksize=5000)

    